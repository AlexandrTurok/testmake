TARGET = hello
OBJ_DIR = obj

.PHONY: all clean install uninstall

all: hello

clean:
		rm -rf $(TARGET) obj/*.o

$(OBJ_DIR)/%.o: src/%.c
		cc -c $< -o $@

$(TARGET): $(OBJ_DIR)/main.o obj/hello.o
		cc -o $(TARGET) $(OBJ_DIR)/main.o $(OBJ_DIR)/hello.o

install:
		install $(TARGET) /home/aleksandr/workspace/bin

uninstall:
		rm -rf $(TARGET) /home/aleksandr/workspace/bin/hello
